import random
from pathlib import Path


def isFile(filePath):
    """Checks if the given filepath points to an existing file

    Arguments
    ---------
    filePath : str
        The path pointing to the file being checked.

    Returns
    -------
    path.is_file() : bool
        A boolean where True indicates that the file exists and False indicates
        that it does not.
    """
    path = Path(filePath)
    return path.is_file()


def writeDefaultSettingsFile():
    """Writes the default settings settings file, creating it
    if it doesn't exist.

    The settings file will look like:
    1
    1
    """
    with open("settings", "w") as settingsFile:
        settingsFile.write("1\n1")


def checkSettingsFile():
    """Checks if the settings file exists, creates it with default values
    if it doesn't.
    """
    if not isFile("settings"):
        writeDefaultSettingsFile()


def readSettings(settingsNo):
    """Reads the settings file then parses and returns the setting requested.

    If the file doesn't exist it is created with default values and if the given
    setting's option is in an incorrect format it will also be reset to default
    values.

    Arguments
    ---------
    settingsNo : int
        The number associated with settings option wanted. Starts from 0.

    Returns
    -------
    settingsOption : int
        The integer found when parsing the specified option within the settings
        file, or a 1 (default setting) if an issue was found with the file.
    """
    checkSettingsFile()
    with open("settings", "r") as settingsFile:
        try:
            settingsOption = int(settingsFile.read().split("\n")[settingsNo])
        except Exception:
            print(
                "An issue with the settings file was detected.\n"
                "Returning it to its default values."
            )
            writeDefaultSettingsFile()
            settingsOption = 1
    return settingsOption


def writeSettings(settings):
    """Writes given lines to settings file

    Arguments
    ---------
    settings : list of integers
        A list of integers to be written as seperate lines to the settings file.
    """
    with open("settings", "w") as settingsFile:
        settingsFile.writelines("%s\n" % str(s) for s in settings)


def settingsThreeOptions(title, options):
    """Allows the user to pick which settings option to interact with.

    Outputs the current settings menu option and then asks the user to input
    which option they would like to interact with. If they enter an invalid
    option then it reasks them until a valid option is given.

    Arguments
    ---------
    title : str
        The title/name of the current settings menu. Printed above the options.

    options : list of 3 strings
        A list of length 3 (>3 is allowed but all other strings will be ignored)
        containing the names of the setting menu's options.

    Returns
    -------
    int(settingsSelect)-1 : int
        The selected setting option given as a value
        between 0 and 2 (inclusive).
    """
    print(title)
    for i in range(3):
        print(f"{i+1}. {options[i]}")
    while True:
        settingsSelect = input("Enter the number of the setting option you'd like: ")
        if (
            settingsSelect.isdigit()
            and int(settingsSelect) > 0
            and int(settingsSelect) <= 3
        ):
            return int(settingsSelect) - 1
        print("Incorrect input (Enter 1-3)")


def settings():
    """Used to access and change the generator's settings.

    This first reads then prints the current settings and then allows
    the user to select and change the settings. Any settings changes are
    rewritten to the settings file.
    """
    formatOptions = ("no formatting", "all uppercase", "all lowercase")
    spaceOptions = (
        "keep words seperate",
        "keep spaces in same place",
        "treat spaces like all other characters",
    )
    while True:
        settingsList = []
        settingsList.append(readSettings(0))
        settingsList.append(readSettings(1))
        print(
            "Current settings\n"
            f"1. Format phrase: {formatOptions[settingsList[0]]}\n"
            f"2. Space handling: {spaceOptions[settingsList[1]]}"
        )
        settingsInput = input(
            "Enter the number of what you'd like to change"
            " or 'q' to quit the settings: "
        ).lower()
        if settingsInput == "q":
            break
        if settingsInput == "1":
            settingsList[0] = settingsThreeOptions("Format options", formatOptions)
        elif settingsInput == "2":
            settingsList[1] = settingsThreeOptions("Space handling", spaceOptions)
        writeSettings(settingsList)


def applyFormatSetting(inputText):
    """Formats the inputted string using the settings in the settings file.

    Arguments
    ---------
    inputText : str

    Returns
    -------
    inputText OR inputText.upper() OR inputText.lower() : str
        The inputted input text formatted according to the settings.
    """
    formatSetting = readSettings(0)
    if formatSetting == 0:
        return inputText
    if formatSetting == 1:
        return inputText.upper()
    if formatSetting == 2:
        return inputText.lower()
    print(
        "An issue with the settings file was detected"
        ", returning it to its default values"
    )
    writeDefaultSettingsFile()
    return inputText.upper()


def generate(inputText):
    """Generates and returns a shuffled version of the given text"""
    inputLen = len(inputText)
    anagram = ""
    for character in range(0, inputLen):
        charTake = random.randint(0, inputLen - 1 - character)
        anagram += inputText[charTake]
        inputText = inputText[:charTake] + inputText[charTake + 1 :]
    return anagram


def keepSeperate(inputText):
    """Generates an anagram for each section of string split by a space.

    Abstractly:
    The string is first split into all substrings seperated by a space.
    e.g. "Hello World!" -> ["Hello", "World!"]
    Each individual string is then turned into an anagram.
    e.g. ["Hello", "World!"] -> ["elHlo", "or!lWd"]
    The substrings are then concatenated, with
    the space being reintroduced and returned.
    e.g. ["elHlo", "or!lWd"] -> "elHlo or!lWd"

    Arguments
    ---------
    inputText : str

    Returns
    -------
    anagram.strip(" ") : str
        Returns the generated anagram, removing a spare
        space from the beginning of the string.
    """
    wordList = inputText.split(" ")
    anagram = ""
    for word in wordList:
        anagram += " " + generate(word)
    return anagram.strip(" ")


def keepSpaces(inputText):
    """Generates an anagram using all the string retaining whitespace positions.

    This function generates the anagram using all non-whitespace characters
    within the inputted text whilst returning the positioning of the spaces
    within the string.
    e.g. "1234 56 789" -> "9641 28 357"

    Arguments
    ---------
    inputText : str

    Returns
    -------
    anagram : str
        The generated anagram string, including the spaces.
    """
    spacesNos = []
    for charNo in range(len(inputText) - 1, -1, -1):
        if inputText[charNo] == " ":
            spacesNos.append(charNo)
            inputText = inputText[:charNo] + inputText[charNo + 1 :]
    anagram = generate(inputText)
    for charNo in spacesNos:
        anagram = anagram[:charNo] + " " + anagram[charNo:]
    return anagram


def generateFull(inputText):
    """Generates the anagram according to the settings in the settings file."""
    keepSeperateSetting = readSettings(1)
    if keepSeperateSetting == 0:
        return keepSeperate(inputText)
    if keepSeperateSetting == 1:
        return keepSpaces(inputText)
    if keepSeperateSetting == 2:
        return generate(inputText)
    print(
        "An issue with the settings file was detected"
        ", returning it to its default values"
    )
    writeDefaultSettingsFile()
    return keepSpaces(inputText)


while True:
    phrase = input(
        "Enter your word, phrase, '-s' for settings or '-q' to quit: "
    ).strip()
    if phrase == "-q":
        break
    elif phrase == "-s":
        settings()
    else:
        anagram = generateFull(applyFormatSetting(phrase))
        print(anagram)
