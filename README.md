# Anagram generator

An anagram generator written in Python.

##### Status: Finished

## Functionality

When run, the program will ask for an input.

Enter `-s` to enter the settings menu which allows you to change the way the program handles inputs. Settings changes persist across sessions.
